//
//  QuizAppApp.swift
//  QuizApp
//
//  Created by Andrei Albu on 12.10.2023.
//

import SwiftUI

@main
struct QuizAppApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
